const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const NoteSchema = new Schema(
  {
    noteContent: { type: String, required: true },
    geometry: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number] }
    },
    locAddress: { type: String },
    poster: { type: ObjectId, ref: "User", required: true },
    deleted: { type: Number, required: true, default: 0 }
  },
  { timestamps: true }
);
mongoose.model("Note", NoteSchema);
module.exports = mongoose.model("Note");
