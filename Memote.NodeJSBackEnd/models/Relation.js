const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const RelationSchema = new Schema(
  {
    follower: { type: ObjectId, ref: "User", required: true },
    following: { type: ObjectId, ref: "User", required: true }
  },
  { timestamps: true }
);

mongoose.model("Relation", RelationSchema);
module.exports = mongoose.model("Relation");
