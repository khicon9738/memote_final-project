const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const CommentSchema = new Schema(
  {
    commentContent: { type: String, required: true },
    poster: { type: ObjectId, ref: "User", required: true },
    note: { type: ObjectId, ref: "Note", required: true },
    deleted: { type: Number, required: true, default: 0 }
  },
  { timestamps: true }
);

mongoose.model("Comment", CommentSchema);
module.exports = mongoose.model("Comment");
