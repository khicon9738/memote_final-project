const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const MediaSchema = new Schema(
  {
    fileName: { type: String, require: true },
    caption: { type: String },
    note: { type: ObjectId, ref: "Note" }
  },
  { timestamps: true }
);

mongoose.model("Media", MediaSchema);
module.exports = mongoose.model("Media");
