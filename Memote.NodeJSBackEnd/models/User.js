const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: email => {
          return /[\w]+@[\w]+.[\w]+/gi.test(email);
        },
        message: "Email not valid"
      }
    },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    dob: { type: String, require: true },
    gender: { type: Number, required: true },
    phone: { type: String, unique: true, required: true },
    active: { type: Number, required: true, default: 1 }
  },
  { timestamps: true }
);

mongoose.model("User", UserSchema);

module.exports = mongoose.model("User");
