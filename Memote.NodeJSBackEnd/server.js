const app = require("./app");
const port = process.env.port || 4200;

const server = app.listen(port, () => {
  console.log("Memote server is listening on port " + port);
});
