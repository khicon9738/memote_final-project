const authController = require("../controllers/AuthController");
const userModel = require("../models/User");

const verifyRequest = async (req, res, next) => {
  try {
    let token = req.headers["x-access-token"];
    if (!token) {
      res.status(401).send({ result: "failed", message: "Unauthorized request." });
    }
    let decoded = await authController.verifyJWT(token);
    let user = await userModel.findById(decoded.id);
    if (!user) {
      res.send(401).send({ result: "failed", message: "Invalid token" });
    } else {
      req.body.userId = user.id;
      next();
    }
  } catch (err) {
    res.status(404).send({ result: "failed", message: err.message });
  }
};

module.exports = { verifyRequest };
