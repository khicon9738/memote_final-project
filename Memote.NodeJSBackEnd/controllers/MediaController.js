const mediaModel = require("../models/Media");
const formidable = require("formidable");

const uploadMedia = async data => {
  let result = await promiseUploadImg(data);
  return result;
};

const promiseUploadImg = data => {
  return new Promise((resolve, reject) => {
    let fileForm = formidable.IncomingForm();
    fileForm.encoding = "utf-8";
    fileForm.uploadDIr = "./upload/image";
    fileForm.keepExtensions = true;
    fileForm.maxFieldsSize = 25 * 1024 * 1024;
    fileForm.maxFileSize = 25 * 1024 * 1024;
    fileForm.maxFields = 1;
    fileForm.multiples = false;

    fileForm.parse(data, async (err, fields, files) => {
      if (err) {
        reject({ result: 'failed', message: "Unable to upload file.Error: " + err.message });
      } else {
        let file = files.uploadImage;
        if (file && file !== "null" && file !== "undefined") {
          let fileName = file.path.replace(/\\/g, "/");
          fileName = fileName.split("/")[2];

          let newFile = {
            fileName: fileName
          };
          await mediaModel.create(newFile);
          resolve({ result: "success", message: "Upload file successfully." });
        } else {
          reject({ result: 'failed', message: "No file to upload!" });
        }
      }
    });
  });
};

module.exports = {
  uploadMedia
};