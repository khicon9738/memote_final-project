const relationModel = require('../models/Relation');

const userFollow = async data => {
    let newRelation = {
        follower: data.userId,
        following: data.following
    }
    await relationModel.create(newRelation);
    return { result: "success", message: "Followed" };
};

const userUnfollow = async condition => {
    await relationModel.findOneAndDelete(condition);
    return { result: "success", message: "Unfollowed" };
};

const findRelation = async condition => {
    let relation = await relationModel.findOne(condition);
    if (relation) {
        return { result: true };
    } else {
        return { result: false };
    }
};

module.exports = {
    userFollow,
    userUnfollow,
    findRelation
};