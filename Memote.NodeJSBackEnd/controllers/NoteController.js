const noteModel = require("../models/Note");

//Public method
const createNote = async data => {
  let geometryObj = await JSON.parse(data.geometry);
  let newNote = {
    noteContent: data.noteContent,
    geometry: geometryObj,
    locAddress: data.locAddress,
    poster: data.userId
  };
  await noteModel.create(newNote);
  return { result: "success", message: "New note posted!" };
};

const getGeoNote = async (location, projection, selectedField) => {
  let notes = await noteModel
    .find(
      {
        geometry: {
          $nearSphere: {
            $geometry: {
              type: "Point",
              coordinates: [parseFloat(location.long), parseFloat(location.lat)]
            },
            $maxDistance: location.maxDis
          }
        },
        deleted: 0
      },
      projection
    )
    .populate("poster", selectedField)
    .sort({ createdAt: -1 })
    .exec();
  return { result: "success", note: notes };
};

const getNote = async (populate, condition, projection, selectedField) => {
  if (populate) {
    let notes = await noteModel
      .find(condition, projection)
      .populate("poster", selectedField)
      .sort({ createdAt: -1 })
      .exec();
    return { result: "success", note: notes };
  } else {
    let notes = await noteModel
      .find(condition, projection)
      .exec();
    return { result: "success", note: notes };
  }
};

const getNoteDetail = async (noteId, projection, selectedField) => {
  let note = await noteModel
    .findById(noteId, projection)
    .populate("poster", selectedField)
    .exec();
  if (!note) {
    throw { result: "failed", message: "Note not found!" };
  } else {
    return { result: "success", note: note };
  }
};

const updateNote = async (noteId, data, option) => {
  let updatedNote = await noteModel.findByIdAndUpdate(noteId, data, option);
  return {
    result: "success",
    message: "Update note successfully.",
    updatedNote: updatedNote
  };
};

module.exports = {
  createNote,
  getGeoNote,
  getNote,
  getNoteDetail,
  updateNote
};
