const commentModel = require('../models/Comment');

//Public method
const createComment = async data => {
    let comment = {
        commentContent: data.commentContent,
        poster: data.userId,
        note: data.note
    };

    let newComment = await commentModel.create(comment);
    return { result: "success", message: "New comment posted", commentId: newComment.id };
};

//Get note comments
const getNoteComments = async (condition, projection, selectedField) => {
    let comments = await commentModel
        .find(condition, projection)
        .populate("poster", selectedField)
        .exec();
    return { result: "success", comment: comments };
};

//Delete comment
const deleteComment = async commentId => {
    await commentModel.findByIdAndUpdate(commentId, { deleted: 1 });
    return { result: "success", message: "Comment is deleted" };
};

module.exports = {
    createComment,
    getNoteComments,
    deleteComment
};