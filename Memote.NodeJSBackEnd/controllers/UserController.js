const bcryptjs = require("bcryptjs");
const userModel = require("../models/User");
const config = require("../config");
const authController = require("./AuthController");

//Public method
const createUser = async user => {
  let hasedPassword = await hashPassword(user.password);

  let tempUser = {
    username: user.username,
    password: hasedPassword,
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    dob: user.dob,
    gender: user.gender,
    phone: user.phone
  };
  let newUser = await userModel.create(tempUser);
  return {
    result: "success",
    message: "User " + newUser.username + " created!"
  };
};

const loginUser = async loginInfo => {
  let loginusername = loginInfo.username;
  let user = await userModel.findOne({ username: loginusername });

  if (!user) {
    throw { result: "Login fail", message: "Username doesn't exist." };
  } else {
    let isPasswordValid = await bcryptjs.compare(
      loginInfo.password.concat(config.pwd),
      user.password
    );
    if (!isPasswordValid) {
      throw { result: "Login failed", message: "Wrong password." };
    } else {
      let token = await authController.createJWT(user.id);
      return { result: "success", token: token };
    }
  }
};

const userDetail = async (condition, projection) => {
  let user = await userModel.findOne(condition, projection);
  if (!user) {
    throw { result: "failed", message: "No user exist" };
  } else {
    return { result: "success", user: user };
  }
};

const updateUser = async (condition, data, option) => {
  let updatedUser = await userModel.findByIdAndUpdate(condition, data, option);
  return {
    result: "success",
    message: "Update user " + updatedUser.username + " successfully.",
    updatedUser: updatedUser
  };
};

const updatePassword = async data => {
  let user = await userModel.findById(data.userId);
  let isPasswordValid = await bcryptjs.compare(
    data.currentPassword.concat(config.pwd),
    user.password
  );
  if (!isPasswordValid) {
    throw { result: "failed", message: "Current password is not correct." };
  } else {
    let hasedPassword = await hashPassword(data.newPassword);
    user.password = hasedPassword;
    await user.save();
    return { result: "success", message: "Update password successfully." };
  }
};

const searchUser = async (condition, projection) => {
  let users = await userModel.find(
    condition,
    projection
  );
  return { result: "success", user: users };
};

//Private method
const hashPassword = async password => {
  let salt = await bcryptjs.genSalt(config.salt);
  let hasedPassword = await bcryptjs.hash(password.concat(config.pwd), salt);
  return hasedPassword;
};

module.exports = {
  createUser,
  loginUser,
  userDetail,
  updateUser,
  updatePassword,
  searchUser
};
