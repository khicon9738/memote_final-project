const jwt = require("jsonwebtoken");
const key = require("../config").secretKey;

//Create jwt
const createJWT = async userId => {
  let token = await jwt.sign({ id: userId }, key, {
    algorithm: "HS512",
    expiresIn: "30 days"
  });
  return token;
};

//Verify token
const verifyJWT = async token => {
  let decoded = await jwt.verify(token, key);
  return decoded;
};
module.exports = {
  createJWT,
  verifyJWT
};
