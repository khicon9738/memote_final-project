/* eslint-disable no-unused-vars */
const express = require("express");
const app = express();
const db = require("./db");
const bodyPaser = require("body-parser");
const authenMiddleware = require("./middlewares/AuthMiddleware");

//Middleware
app.use(bodyPaser.urlencoded({ extended: true }));
app.use(bodyPaser.json({ extended: true }));

//Route
const UserRoute = require("./routes/UserRouter");
app.use("/api/users", UserRoute);
const NoteRoute = require("./routes/NoteRouter");
app.use("/api/notes", authenMiddleware.verifyRequest, NoteRoute);
const CommentRoute = require("./routes/CommentRouter");
app.use("/api/comments", authenMiddleware.verifyRequest, CommentRoute);
const RelationRoute = require("./routes/RelationRouter");
app.use("/api/relations", authenMiddleware.verifyRequest, RelationRoute);
const MediaRoute = require('./routes/MediaRouter');
app.use("/api/media", MediaRoute);

module.exports = app;
