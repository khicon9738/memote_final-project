const express = require('express');
const router = express.Router();
const commentController = require("../controllers/CommentController");

//Post comment
router.post("/post-comment", async (req, res) => {
    try {
        let result = await commentController.createComment(req.body);
        console.log("Note " + req.body.note + " is just got commented");
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

//Get note comments
router.get("/get-comments", async (req, res) => {
    try {
        let result = await commentController.getNoteComments(
            { note: req.query.noteId, deleted: 0 },
            { deleted: 0, updatedAt: 0, __v: 0 },
            { _id: 1, firstName: 1, lastName: 1 }
        );
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

//Delete comment
router.delete("/delete-comment/:commentId", async (req, res) => {
    try {
        let result = await commentController.deleteComment(req.params.commentId);
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

module.exports = router;