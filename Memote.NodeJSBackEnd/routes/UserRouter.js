/* eslint-disable no-console */
const express = require("express");
const router = express.Router();
const userController = require("../controllers/UserController");
const authController = require("../controllers/AuthController");
const verifyRequest = require("../middlewares/AuthMiddleware").verifyRequest;

//Verify token
router.post("/verify-jwt", async (req, res) => {
  try {
    console.log("Verifing jwt...");
    let token = req.headers["x-access-token"];
    if (!token) {
      res.status(401).send({ auth: false, message: "No token provided." });
    }
    await authController.verifyJWT(token);
    console.log("JWT is valid");
    res.status(200).send({ result: "success", message: "Token valid" });
  } catch (err) {
    console.log("JWT is invalid");
    res.status(404).send({ result: "failed", message: err.message });
  }
});

//Create new user
router.post("/register", async (req, res) => {
  try {
    let result = await userController.createUser(req.body);
    console.log(result.message);
    res.status(201).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Login
router.post("/login", async (req, res) => {
  try {
    let result = await userController.loginUser(req.body);
    console.log("User - Login: " + req.body.username);
    res.status(202).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get user detail
router.get("/user-detail", verifyRequest, async (req, res) => {
  try {
    let result = await userController.userDetail(
      { _id: req.body.userId, active: 1 },
      { password: 0, active: 0, createdAt: 0, updatedAt: 0, __v: 0 }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Update user info
router.put("/update-detail", verifyRequest, async (req, res) => {
  try {
    let result = await userController.updateUser(
      req.body.userId,
      req.body,
      {
        new: true,
        projection: { password: 0, active: 0, createdAt: 0, updatedAt: 0, __v: 0 }
      }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Change password
router.put("/change-password", verifyRequest, async (req, res) => {
  try {
    let result = await userController.updatePassword(req.body);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/search", verifyRequest, async (req, res) => {
  try {
    let regex = new RegExp(req.query.keyword, "i");
    let result = await userController.searchUser(
      {
        active: 1,
        $or: [{ firstName: regex }, { lastName: regex }, { email: regex }, { phone: regex }]
      },
      { username: 0, password: 0, active: 0, createdAt: 0, updatedAt: 0, __v: 0 }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
