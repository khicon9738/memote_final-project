const express = require("express");
const router = express.Router();
const relationController = require("../controllers/RelationController");

//Follow route
router.post("/follow", async (req, res) => {
    try {
        let result = await relationController.userFollow(req.body);
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

//Check relation
router.get("", async (req, res) => {
    try {
        let result = await relationController.findRelation({
            follower: req.body.userId,
            following: req.query.following
        });
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

//Unfollow route
router.delete("/unfollow/:following", async (req, res) => {
    try {
        let result = await relationController.userUnfollow({
            follower: req.body.userId,
            following: req.params.following
        });
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

module.exports = router;