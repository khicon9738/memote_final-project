const express = require("express");
const router = express.Router();
const noteController = require("../controllers/NoteController");

//Post new note
router.post("/pin-note", async (req, res) => {
  try {
    let result = await noteController.createNote(req.body);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get all note
router.get("/load-notes", async (req, res) => {
  try {
    let result = await noteController.getGeoNote(
      req.query,
      { deleted: 0, updatedAt: 0, __v: 0 },
      { _id: 1, firstName: 1, lastName: 1 }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get notes of one user
router.get("/user-notes", async (req, res) => {
  try {
    let result = await noteController.getNote(
      true,
      { poster: req.body.userId, deleted: 0 },
      { deleted: 0, updatedAt: 0, __v: 0 },
      { _id: 1, firstName: 1, lastName: 1 }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get note detail
router.get('/:noteId', async (req, res) => {
  try {
    let result = await noteController.getNoteDetail(
      req.params.noteId,
      { deleted: 0, updatedAt: 0, __v: 0 },
      { _id: 1, firstName: 1, lastName: 1 }
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Update note
router.put('/update-note/:noteId', async (req, res) => {
  try {
    let result = await noteController.updateNote(
      req.params.noteId,
      req.body,
      {
        new: true,
        projection: { noteContent: 1 }
      }
    );
    console.log("Note " + req.params.noteId + " is updated.");
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Delete note
router.delete('/delete-note/:noteId', async (req, res) => {
  try {
    let result = await noteController.updateNote(
      req.params.noteId,
      { deleted: 1 },
      { new: false, upsert: false }
    );
    result.message = "Note is deleted.";
    console.log("Note " + req.params.noteId + " is deleted.");
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
