const express = require('express');
const router = express.Router();
const mediaController = require('../controllers/MediaController');

//Upload media
router.post('/upload', async (req, res) => {
    try {
        let result = await mediaController.uploadMedia(req);
        res.status(200).send(result);
    } catch (err) {
        res.status(500).send(err);
    }
});

module.exports = router;