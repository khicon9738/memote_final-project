package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.ILogin;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;

public class LoginPresenter implements ILogin.Presenter {

    private ILogin.View loginView;
    private PersistentCookieStore tokenStore;

    public LoginPresenter(ILogin.View loginView) {
        this.loginView = loginView;
        tokenStore = new PersistentCookieStore(loginView.requestContext());
    }

    @Override
    public void loginUser(String username, String password) {
        boolean valid = true;

        if (username.trim().isEmpty()) {
            loginView.showUsernameError("Username cannot be empty");
            valid = false;
        }

        if (password.trim().isEmpty()) {
            loginView.showPasswordError("Password cannot be empty");
            valid = false;
        }

        if (valid) {
            loginView.showUsernameError(null);
            loginView.showPasswordError(null);

            RequestParams params = new RequestParams();
            params.add("username", username);
            params.add("password", password);

            loginRequest(params);
        }
    }

    private void loginRequest(RequestParams params) {
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);

        MemoteRestClient client = new MemoteRestClient(headers);
        client.setCookieStore(tokenStore);
        client.postMethod(null, Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.loginEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                loginView.showUsernameError(null);
                loginView.showPasswordError(null);
                loginView.showProgressBar();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String token = response.getString("token");

                    BasicClientCookie newCookie = new BasicClientCookie("x-access-token", token);
                    newCookie.setVersion(1);
                    newCookie.setDomain("http://54.169.251.218:4200");
                    newCookie.setPath("/");
                    tokenStore.addCookie(newCookie);

                    loginView.hideProgressBar();
                    loginView.showResultToast("Login success");
                    loginView.redirectToHomePage();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                try {
                    loginView.hideProgressBar();
                    String message = errorResponse.getString("message");
                    if (message.toLowerCase().contains("username")) {
                        loginView.showUsernameError(message);
                    }
                    if (message.toLowerCase().contains("password")) {
                        loginView.showPasswordError(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
