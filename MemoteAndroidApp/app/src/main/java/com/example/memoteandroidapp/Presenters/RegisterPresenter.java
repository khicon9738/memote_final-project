package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.Enum.Gender;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IRegister;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class RegisterPresenter implements IRegister.Presenter {

    private IRegister.View registerView;

    public RegisterPresenter(IRegister.View registerView) {
        this.registerView = registerView;
    }

    @Override
    public void registerUser(String username, String password, String rePassword, String email, String firstName, String lastName, String dob, String gender, String phone) {
        boolean valid = true;
        if (username.trim().isEmpty()) {
            registerView.setUsernameError();
            valid = false;
        }
        if (password.trim().isEmpty()) {
            registerView.setPasswordError();
            valid = false;
        }
        if (rePassword.trim().isEmpty()) {
            registerView.setRePasswordError();
            valid = false;
        }
        if (email.trim().isEmpty()) {
            registerView.setEmailError();
            valid = false;
        }
        if (email.trim().isEmpty()) {
            registerView.setUsernameError();
            valid = false;
        }
        if (firstName.trim().isEmpty()) {
            registerView.setFirstNameError();
            valid = false;
        }
        if (lastName.trim().isEmpty()) {
            registerView.setLastNameError();
            valid = false;
        }
        if (dob.trim().isEmpty()) {
            registerView.setDobError();
            valid = false;
        }
        if (phone.trim().isEmpty()) {
            registerView.setPhoneError();
            valid = false;
        }
        if (!rePassword.trim().equals(password.trim())) {
            registerView.setRePasswordError();
            valid = false;
        }

        if (valid) {
            RequestParams param = new RequestParams();
            param.add("username", username);
            param.add("password", password);
            param.add("email", email);
            param.add("firstName", firstName);
            param.add("lastName", lastName);
            param.add("dob", dob);
            param.add("gender", Gender.valueOf(gender).getGenderCode());
            param.add("phone", phone);
            registerRequest(param);
        }
    }

    private void registerRequest(RequestParams newUser) {
        //Create header list
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);

        //New request.
        MemoteRestClient client = new MemoteRestClient(headers);
        client.postMethod(null, Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.registerEndpoint, newUser, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                registerView.showProcessDialog();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String message = null;
                try {
                    message = response.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                registerView.hideProcessDialog();
                registerView.showResultToast(message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                registerView.hideProcessDialog();
                registerView.showResultDialog("Sign Up failed");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                registerView.hideProcessDialog();
                registerView.showResultDialog("Sign Up failed");
            }
        });
    }
}
