package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IUserDetail;
import com.example.memoteandroidapp.Models.Note;
import com.example.memoteandroidapp.Models.User;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class UserDetailPresenter implements IUserDetail.Presenter {

    private IUserDetail.View userDetailView;
    private PersistentCookieStore cookieStore;

    public UserDetailPresenter(IUserDetail.View userDetailView) {
        this.userDetailView = userDetailView;
        cookieStore = new PersistentCookieStore(userDetailView.requestContext());
    }

    @Override
    public void logOut() {
        cookieStore.clear();
        CurrentSession.currentUser = null;
        userDetailView.showResultToast("Logout successfully");
        userDetailView.redirectLogin();
    }

    @Override
    public void getUserDetail() {
        userDetailRequest();
    }

    @Override
    public void getUserNotes() {
        CurrentSession.userNotes.clear();
        userNotesRequest();
    }

    private void userDetailRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(userDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.userDetailEndpoint, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject userObject = response.getJSONObject("user");
                    User user = User.convertJSONObjectToUser(userObject);

                    CurrentSession.currentUser = user;

                    userDetailView.setUserFullName(user.getFullName());
                    userDetailView.setUserEmail(user.getEmail());
                    userDetailView.setUserBirthday(user.getDob());
                    userDetailView.setUserGender(user.getGender().toString());
                    userDetailView.setUserPhone(user.getPhone());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

            }
        });
    }

    private void userNotesRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(userDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + Endpoints.userNoteEndpoint, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray notes = response.getJSONArray("note");
                    List<Note> noteList = Note.convertJSONArrayToList(notes);

                    CurrentSession.userNotes.addAll(noteList);

                    userDetailView.userNoteDataChanged();
                    userDetailView.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
