package com.example.memoteandroidapp.Common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Converter {

    public static String convertISOToLongDate(String isoTime) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date currentDate = new Date();
            Date convertedDate = df.parse(isoTime);
            long diffDays = TimeUnit.DAYS.convert(Math.abs(currentDate.getTime() - convertedDate.getTime()), TimeUnit.MILLISECONDS);

            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yy");
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

            if (diffDays == 0) {
                return "Today at " + timeFormat.format(convertedDate);
            } else {
                return dateFormat.format(convertedDate) + " at " + timeFormat.format(convertedDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return isoTime;
        }
    }

    public static String convertISOToShortDate(String isoTime) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date currentDate = new Date();
            Date convertedDate = df.parse(isoTime);
            long diffDays = TimeUnit.DAYS.convert(Math.abs(currentDate.getTime() - convertedDate.getTime()), TimeUnit.MILLISECONDS);
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yy");

            if (diffDays == 0) {
                return timeFormat.format(convertedDate);
            } else {
                return dateFormat.format(convertedDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return isoTime;
        }
    }
}
