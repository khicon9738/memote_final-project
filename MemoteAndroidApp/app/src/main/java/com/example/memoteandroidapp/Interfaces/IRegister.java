package com.example.memoteandroidapp.Interfaces;

public interface IRegister {
    interface View {
        void setUsernameError();
        void setPasswordError();
        void setRePasswordError();
        void setEmailError();
        void setFirstNameError();
        void setLastNameError();
        void setDobError();
        void setPhoneError();
        void showProcessDialog();
        void hideProcessDialog();
        void showResultToast(String result);
        void showResultDialog(String result);
    }

    interface Presenter {
        void registerUser(String username, String password,String rePassword, String email, String firstName, String lastName, String dob, String gender, String phone);
    }
}
