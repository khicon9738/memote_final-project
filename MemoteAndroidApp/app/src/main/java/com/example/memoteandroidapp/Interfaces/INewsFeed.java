package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

public interface INewsFeed {
    interface View {
        void setRefreshing(boolean status);
        void newsFeedNoteChanged();
        Context requestContext();
        Activity requestActivity();
    }

    interface Presenter {
        void getNewsFeedNoteList();
    }
}
