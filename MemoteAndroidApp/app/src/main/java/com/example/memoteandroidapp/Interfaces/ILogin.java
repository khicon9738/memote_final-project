package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface ILogin {
    interface View{
        void showUsernameError(String error);
        void showPasswordError(String error);
        void showProgressBar();
        void hideProgressBar();
        void redirectToRegister(android.view.View view);
        void redirectToHomePage();
        void showResultToast(String message);
        Context requestContext();
    }
    interface Presenter{
        void loginUser(String username, String password);
    }
}
