package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IEdit;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class EditPresenter implements IEdit.Presenter {

    private IEdit.View editView;
    private int createOption;
    private PersistentCookieStore cookieStore;

    public EditPresenter(IEdit.View editView, int option) {
        this.editView = editView;
        this.createOption = option;
        cookieStore = new PersistentCookieStore(this.editView.requestContext());
    }

    @Override
    public void setDisplayContent() {
        if (createOption == Constants.CREATE_OPTION_EDIT_NOTE) {
            editView.setEditContent(CurrentSession.currentNote.getNoteContent());
        }
        if (createOption == Constants.CREATE_OPTION_EDIT_COMMENT) {

        }
    }

    @Override
    public void updateContent(String content) {
        editView.showContentError(null);
        boolean valid = true;

        if (content.trim().isEmpty()) {
            editView.showContentError("Content cannot be empty.");
            valid = false;
        }

        if (valid) {
            editView.showContentError(null);

            if (createOption == Constants.CREATE_OPTION_EDIT_NOTE) {
                RequestParams params = new RequestParams();
                params.add("noteContent", content);
                updateNoteContentRequest(params);
            }
            if (createOption == Constants.CREATE_OPTION_EDIT_COMMENT) {

            }
        }
    }

    private void updateNoteContentRequest(RequestParams params) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.putMethod(editView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + Endpoints.editNoteEndpoint + CurrentSession.currentNote.getNoteId(), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                editView.showProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String result = response.getString("message");
                    editView.hideProgressDialog();
                    editView.showToastResult(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateCommentContentRequest(RequestParams params) {

    }
}
