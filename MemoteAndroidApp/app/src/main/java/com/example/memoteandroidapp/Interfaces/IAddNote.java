package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

import org.json.JSONObject;

public interface IAddNote {
    interface View{
        void showNoteContentError(String error);
        void setCurrentLocation(String location);
        void showToastResult(String result);
        Context requestContext();
        Activity requestActivity();
    }

    interface Presenter{
        void addNote(String currentLoc, String noteContent);
    }
}
