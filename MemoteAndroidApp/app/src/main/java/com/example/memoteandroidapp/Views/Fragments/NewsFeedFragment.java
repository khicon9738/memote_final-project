package com.example.memoteandroidapp.Views.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.memoteandroidapp.Adapters.NoteAdapter;
import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.INewsFeed;
import com.example.memoteandroidapp.Presenters.NewsFeedPresenter;
import com.example.memoteandroidapp.R;

import java.util.ArrayList;

public class NewsFeedFragment extends Fragment implements INewsFeed.View {

    private View view;
    private INewsFeed.Presenter newsFeedPresenter;
    private RecyclerView newsFeedNoteList;
    private NoteAdapter noteAdapter;
    private SwipeRefreshLayout refreshNoteList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news_feed, container, false);
        newsFeedPresenter = new NewsFeedPresenter(this);
        CurrentSession.newsFeedNotes = new ArrayList<>();

        newsFeedNoteList = view.findViewById(R.id.newsFeedNoteList);
        noteAdapter = new NoteAdapter(CurrentSession.newsFeedNotes, getActivity(), Constants.NEWSFEED_NOTE_DETAIL_CODE);
        newsFeedNoteList.setLayoutManager(new LinearLayoutManager(getContext()));
        newsFeedNoteList.setAdapter(noteAdapter);

        refreshNoteList = view.findViewById(R.id.newsFeedRefreshList);
        refreshNoteList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                newsFeedPresenter.getNewsFeedNoteList();
            }
        });

        return view;
    }

    @Override
    public void setRefreshing(boolean status) {
        refreshNoteList.setRefreshing(status);
    }

    @Override
    public void newsFeedNoteChanged() {
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    public Context requestContext() {
        return super.getContext();
    }

    @Override
    public Activity requestActivity() {
        return super.getActivity();
    }
}
