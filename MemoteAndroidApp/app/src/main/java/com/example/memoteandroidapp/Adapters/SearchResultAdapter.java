package com.example.memoteandroidapp.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.memoteandroidapp.Models.User;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.ResultDetailActivity;

import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {

    private List<User> searchResults;
    private Activity activity;

    public SearchResultAdapter(List<User> searchResults, Activity activity) {
        this.searchResults = searchResults;
        this.activity = activity;
    }

    public class SearchResultViewHolder extends RecyclerView.ViewHolder {

        private TextView fullName;

        public SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);

            fullName = itemView.findViewById(R.id.searchResultName);
        }
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_result_item, viewGroup, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder searchResultViewHolder, int i) {
        final int resultIndex = i;
        final User user = searchResults.get(i);
        searchResultViewHolder.fullName.setText(user.getFullName());

        searchResultViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultView = new Intent(activity, ResultDetailActivity.class);
                resultView.putExtra("resultIndex", resultIndex);
                activity.startActivity(resultView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }
}
