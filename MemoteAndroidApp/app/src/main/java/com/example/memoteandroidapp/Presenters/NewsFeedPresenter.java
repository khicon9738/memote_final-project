package com.example.memoteandroidapp.Presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.INewsFeed;
import com.example.memoteandroidapp.Models.Note;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class NewsFeedPresenter implements INewsFeed.Presenter {

    private INewsFeed.View newsFeedView;
    private PersistentCookieStore cookieStore;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback requestUpdateCallback;
    private LocationRequest locationRequest;
    private Location currentLocation;
    private String getNoteEndpoint;

    public NewsFeedPresenter(INewsFeed.View newsFeedView) {
        this.newsFeedView = newsFeedView;

        cookieStore = new PersistentCookieStore(this.newsFeedView.requestContext());
        fusedLocationProviderClient = new FusedLocationProviderClient(this.newsFeedView.requestContext());
        locationRequest = new LocationRequest();
        locationRequest.setInterval(60000);
        locationRequest.setFastestInterval(60000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        requestUpdateCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                } else {
                    currentLocation = locationResult.getLastLocation();
                }
            }
        };
        checkPermission();
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, requestUpdateCallback, null);
        requestLastLocation(fusedLocationProviderClient.getLastLocation());
    }

    @Override
    public void getNewsFeedNoteList() {
        CurrentSession.newsFeedNotes.clear();
        checkPermission();
        requestLastLocation(fusedLocationProviderClient.getLastLocation());
    }

    private void requestLastLocation(Task<Location> task) {
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    getNoteEndpoint = String.format(Endpoints.getNoteEndpoint, currentLocation.getLongitude(), currentLocation.getLatitude(), "0", CurrentSession.noteRange);
                    newsFeedNotesRequest();
                }
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                currentLocation = null;
            }
        });
    }

    private void newsFeedNotesRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(newsFeedView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + getNoteEndpoint, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray notes = response.getJSONArray("note");
                    List<Note> noteList = Note.convertJSONArrayToList(notes);

                    CurrentSession.newsFeedNotes.addAll(noteList);

                    newsFeedView.newsFeedNoteChanged();
                    newsFeedView.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(newsFeedView.requestContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(newsFeedView.requestActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.REQUEST_ACCESS_FINE_LOCATION_CODE);
        }
        if (ContextCompat.checkSelfPermission(newsFeedView.requestContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(newsFeedView.requestActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.REQUEST_ACCESS_COARSE_LOCATION_CODE);
        }
    }
}
