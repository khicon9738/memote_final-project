package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface ISearchResult {
    interface View{
        void searchResultNotifyChanged();
        Context requestContext();
    }
    interface Presenter{
        void searchUser(String keyword);
    }
}
