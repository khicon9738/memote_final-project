package com.example.memoteandroidapp.Views.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.memoteandroidapp.Interfaces.IAddNote;
import com.example.memoteandroidapp.Presenters.AddNotePresenter;
import com.example.memoteandroidapp.R;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONObject;
import org.w3c.dom.Text;

import javax.xml.transform.Result;

public class AddNoteFragment extends DialogFragment implements IAddNote.View {

    private View view;
    private TextInputLayout noteContent;
    private IAddNote.Presenter addNotePresenter;
    private EditText currentLocation;
    private Button pinButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_note, null, false);
        addNotePresenter = new AddNotePresenter(this);

        noteContent = view.findViewById(R.id.addNoteContent);
        currentLocation = view.findViewById(R.id.addNoteLocationText);
        pinButton = view.findViewById(R.id.addNotePinButton);

        pinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotePresenter.addNote(
                        currentLocation.getText().toString(),
                        noteContent.getEditText().getText().toString()
                );
            }
        });

        return view;
    }

    @Override
    public void showNoteContentError(String error) {
        noteContent.setError(error);
    }

    @Override
    public void setCurrentLocation(String location) {
        currentLocation.setText(location);
    }

    @Override
    public void showToastResult(String result) {
        Toast resultToast = Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT);
        resultToast.show();
        dismiss();
    }

    @Nullable
    @Override
    public Context requestContext() {
        return super.getContext();
    }

    @Override
    public Activity requestActivity() {
        return super.getActivity();
    }
}
