package com.example.memoteandroidapp.Views;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.memoteandroidapp.Interfaces.INoteSetting;
import com.example.memoteandroidapp.Presenters.NoteSettingPresenter;
import com.example.memoteandroidapp.R;

public class NoteSettingActivity extends AppCompatActivity implements INoteSetting.View {

    private INoteSetting.Presenter noteSettingPresenter;
    private EditText rangeNote;
    private Toolbar toolbar;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_setting);
        noteSettingPresenter = new NoteSettingPresenter(this);

        rangeNote = findViewById(R.id.noteSettingNoteRange);
        saveButton = findViewById(R.id.noteSettingSaveBtn);

        toolbar = findViewById(R.id.noteSettingToolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Note Setting");

        noteSettingPresenter.loadSetting();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setRange(String range) {
        rangeNote.setText(range);
    }

    @Override
    public void setRangeError(String error) {
        rangeNote.setError(error);
    }

    @Override
    public void showResultToast(String result) {
        Toast resultToast = Toast.makeText(this, result, Toast.LENGTH_LONG);
        resultToast.show();
        this.finish();
    }

    public void onClickSaveSetting(View view) {
        noteSettingPresenter.saveSetting(rangeNote.getText().toString());
    }
}
