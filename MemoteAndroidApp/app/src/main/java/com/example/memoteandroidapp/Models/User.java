package com.example.memoteandroidapp.Models;

import com.example.memoteandroidapp.Common.Enum.Gender;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String dob;
    private String phone;
    private Gender gender;

    public User() {

    }

    public User(String userId, String firstName, String lastName, String email, String dob, String phone, Gender gender) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.phone = phone;
        this.gender = gender;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFullName() {
        return this.getFirstName() + " " + getLastName();
    }

    public static User convertJSONObjectToUser(JSONObject data) throws JSONException {
        User user = new User();

        user.setUserId(data.getString("_id"));
        user.setFirstName(data.getString("firstName"));
        user.setLastName(data.getString("lastName"));
        if (data.has("email")) {
            user.setEmail(data.getString("email"));
        }
        if (data.has("dob")) {
            user.setDob(data.getString("dob"));
        }
        if (data.has("phone")) {
            user.setPhone(data.getString("phone"));
        }
        if (data.has("gender")) {
            user.setGender(Gender.values()[data.getInt("gender")]);
        }

        return user;
    }

    public static List<User> convertJSONArrayToList(JSONArray data) throws JSONException {
        List<User> users = new ArrayList<>();
        for (int i = 0; i <= data.length() - 1; i++) {
            User user = User.convertJSONObjectToUser(data.getJSONObject(i));
            users.add(user);
        }
        return users;
    }
}
