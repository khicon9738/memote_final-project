package com.example.memoteandroidapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class Geometry {
    private String type;
    private Double[] coordinates;

    public Geometry() {
    }

    public Geometry(String type, Double[] coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Double[] coordinates) {
        this.coordinates = coordinates;
    }

    public static Geometry convertJSONObjectToGeometry(JSONObject data) throws JSONException {
        Geometry geometry = new Geometry();
        Double[] coordinates = new Double[data.getJSONArray("coordinates").length()];
        for (int i = 0; i <= data.getJSONArray("coordinates").length() - 1; i++) {
            coordinates[i] = data.getJSONArray("coordinates").getDouble(i);
        }

        geometry.setType(data.getString("type"));
        geometry.setCoordinates(coordinates);

        return geometry;
    }
}
