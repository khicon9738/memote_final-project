package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

public interface IUserDetail {
    interface View {
        void setRefreshing(boolean status);
        void userNoteDataChanged();
        void setUserFullName(String fullName);
        void setUserEmail(String email);
        void setUserBirthday(String birthday);
        void setUserGender(String gender);
        void setUserPhone(String phone);
        void redirectLogin();
        void showResultToast(String message);
        Context requestContext();
    }

    interface Presenter {
        void logOut();
        void getUserDetail();
        void getUserNotes();
    }
}
