package com.example.memoteandroidapp.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Models.Comment;
import com.example.memoteandroidapp.R;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private List<Comment> commentList;
    private Activity activity;

    public CommentAdapter(List<Comment> commentList, Activity activity) {
        this.commentList = commentList;
        this.activity = activity;
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        private TextView poster;
        private TextView postedTime;
        private TextView content;
        private LinearLayout item;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.commentPoster);
            postedTime = itemView.findViewById(R.id.commentPostedTime);
            content = itemView.findViewById(R.id.commentContent);
            item = itemView.findViewById(R.id.commentItem);
            item.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            if (CurrentSession.noteComments.get(this.getAdapterPosition()).getPoster().getUserId().equals(CurrentSession.currentUser.getUserId())) {
                menu.add(this.getAdapterPosition(), 0, 0, "Delete");
            }
        }
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View commentItem = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_note_comment, viewGroup, false);
        return new CommentViewHolder(commentItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder commentViewHolder, int i) {
        final Comment comment = commentList.get(i);
        commentViewHolder.poster.setText(comment.getPoster().getFullName());
        commentViewHolder.postedTime.setText(comment.getPostedTime());
        commentViewHolder.content.setText(comment.getCommentContent());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }
}
