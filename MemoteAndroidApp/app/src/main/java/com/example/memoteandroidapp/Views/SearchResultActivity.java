package com.example.memoteandroidapp.Views;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.memoteandroidapp.Adapters.SearchResultAdapter;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.ISearchResult;
import com.example.memoteandroidapp.Presenters.SearchResultPresenter;
import com.example.memoteandroidapp.R;

import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity implements ISearchResult.View {

    private ISearchResult.Presenter searchResultPresenter;
    private Toolbar toolbar;
    private RecyclerView resultList;
    private SearchResultAdapter resultAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        searchResultPresenter = new SearchResultPresenter(this);
        CurrentSession.userSearchResults = new ArrayList<>();

        String data = getIntent().getStringExtra("keyword");

        toolbar = findViewById(R.id.searchResultToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Result");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resultAdapter = new SearchResultAdapter(CurrentSession.userSearchResults, this);
        resultList = findViewById(R.id.searchResultListResult);
        resultList.setLayoutManager(new LinearLayoutManager(this));
        resultList.setAdapter(resultAdapter);

        searchResultPresenter.searchUser(data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                CurrentSession.userSearchResults.clear();
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void searchResultNotifyChanged() {
        resultAdapter.notifyDataSetChanged();
    }

    @Override
    public Context requestContext() {
        return this;
    }
}
