package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface IChangePassword {
    interface View {
        void showCurrentPasswordError(String error);
        void showNewPasswordError(String error);
        void showReNewPasswordError(String error);
        void showProgressDialog();
        void hideProgressDialog();
        void showToastResult(String message);
        void redirectToLogin();
        Context requestContext();
    }

    interface Presenter {
        void changePassword(String currentPassword, String newPassword, String reNewPassword);
    }
}
