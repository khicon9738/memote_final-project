package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Interfaces.IMain;

public class MainPresenter implements IMain.Presenter {

    private IMain.View mainView;

    public MainPresenter(IMain.View view) {
        this.mainView = view;
    }
}
