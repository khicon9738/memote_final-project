package com.example.memoteandroidapp.Interfaces;

public interface INoteSetting {
    interface View {
        void setRange(String range);
        void setRangeError(String error);
        void showResultToast(String result);
    }

    interface Presenter {
        void loadSetting();
        void saveSetting(String newRange);
    }
}
