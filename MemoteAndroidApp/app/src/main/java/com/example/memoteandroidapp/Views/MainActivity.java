package com.example.memoteandroidapp.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.memoteandroidapp.Adapters.ViewPagerAdapter;
import com.example.memoteandroidapp.Interfaces.IMain;
import com.example.memoteandroidapp.Presenters.MainPresenter;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.Fragments.AddNoteFragment;
import com.example.memoteandroidapp.Views.Fragments.NewsFeedFragment;
import com.example.memoteandroidapp.Views.Fragments.UserDetailFragment;

public class MainActivity extends AppCompatActivity implements IMain.View {

    private IMain.Presenter mainPresenter;
    private EditText searchText;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.mainTabLayout);
        viewPager = findViewById(R.id.mainViewPager);
        searchText = findViewById(R.id.mainSearchText);
        searchText.setOnKeyListener(searchTextKey);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.AddFragment(new NewsFeedFragment(), "Notes");
        viewPagerAdapter.AddFragment(new UserDetailFragment(), "User");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);

        mainPresenter = new MainPresenter(this);
    }


    public void onClickNote(View view) {
        DialogFragment addNote = new AddNoteFragment();
        addNote.show(getSupportFragmentManager(), "addNote");
    }

    public void onClickSetting(View vew) {
        Intent noteSetting = new Intent(this, NoteSettingActivity.class);
        startActivity(noteSetting);
    }

    @Override
    public Context requestContext() {
        return this;
    }

    @Override
    public Activity requestActivity() {
        return this;
    }

    public void onClickSearch(View view) {
        if (!searchText.getText().toString().trim().isEmpty()) {
            redirectToSearchResult();
            searchText.getText().clear();
            searchText.clearFocus();
        }
    }

    private View.OnKeyListener searchTextKey = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
                hideKeyboard(v);
                searchText.clearFocus();
                if (!searchText.getText().toString().trim().isEmpty()) {
                    redirectToSearchResult();
                    searchText.getText().clear();
                }
                return true;
            }
            return false;
        }
    };

    private void hideKeyboard(View view) {
        InputMethodManager manager = (InputMethodManager) view.getContext()
                .getSystemService(INPUT_METHOD_SERVICE);
        if (manager != null)
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void redirectToSearchResult() {
        Intent searchResult = new Intent(this, SearchResultActivity.class);
        searchResult.putExtra("keyword", searchText.getText().toString());
        startActivity(searchResult);
    }
}
