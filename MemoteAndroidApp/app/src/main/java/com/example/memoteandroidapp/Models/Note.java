package com.example.memoteandroidapp.Models;

import com.example.memoteandroidapp.Common.Converter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Note {

    private String noteId;
    private String noteContent;
    private Geometry geometry;
    private String locAddress;
    private User poster;
    private String postedTime;

    public Note() {
    }

    public Note(String noteId, String noteContent, Geometry geometry, String locAddress, User poster, String postedTime) {
        this.noteId = noteId;
        this.noteContent = noteContent;
        this.geometry = geometry;
        this.locAddress = locAddress;
        this.poster = poster;
        this.postedTime = postedTime;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getLocAddress() {
        return locAddress;
    }

    public void setLocAddress(String locAddress) {
        this.locAddress = locAddress;
    }

    public User getPoster() {
        return poster;
    }

    public void setPoster(User poster) {
        this.poster = poster;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }

    public static Note convertJSONObjectToNote(JSONObject data) throws JSONException {
        Note note = new Note();

        note.setNoteId(data.getString("_id"));
        note.setNoteContent(data.getString("noteContent"));
        note.setGeometry(Geometry.convertJSONObjectToGeometry(data.getJSONObject("geometry")));
        if (data.has("locAddress")) {
            note.setLocAddress(data.getString("locAddress"));
        } else {
            note.setLocAddress("Memote");
        }
        note.setPostedTime(Converter.convertISOToLongDate(data.getString("createdAt")));
        note.setPoster(User.convertJSONObjectToUser(data.getJSONObject("poster")));

        return note;
    }

    public static List<Note> convertJSONArrayToList(JSONArray data) throws JSONException {
        List<Note> notes = new ArrayList<>();

        if (data.length() == 0) {
            return notes;
        } else {
            for (int i = 0; i <= data.length() - 1; i++) {
                notes.add(Note.convertJSONObjectToNote(data.getJSONObject(i)));
            }
        }

        return notes;
    }
}
