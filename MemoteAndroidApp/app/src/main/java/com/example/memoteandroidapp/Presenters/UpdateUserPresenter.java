package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.Enum.Gender;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IUpdateUser;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class UpdateUserPresenter implements IUpdateUser.Presenter {

    private IUpdateUser.View updateUserView;
    private PersistentCookieStore cookieStore;

    public UpdateUserPresenter(IUpdateUser.View updateUserView) {
        this.updateUserView = updateUserView;
        cookieStore = new PersistentCookieStore(updateUserView.requestContext());
        setCurrentInfo();
    }

    private void setCurrentInfo() {
        String firstName = CurrentSession.currentUser.getFirstName();
        String lastName = CurrentSession.currentUser.getLastName();
        String birthday = CurrentSession.currentUser.getDob();
        String gender = CurrentSession.currentUser.getGender().getGenderCode();
        String phone = CurrentSession.currentUser.getPhone();

        updateUserView.setCurrentInfo(firstName, lastName, birthday, gender, phone);
    }

    @Override
    public void updateUser(String firstName, String lastName, String birthday, String gender, String phone) {
        boolean valid = true;
        if (firstName.trim().isEmpty()) {
            updateUserView.setFirstNameError("First name cannot be empty");
            valid = false;
        }

        if (lastName.trim().isEmpty()) {
            updateUserView.setLastNameError("Last name cannot be empty");
            valid = false;
        }

        if (birthday.trim().isEmpty()) {
            updateUserView.setBirthdayError("Birthday name cannot be empty");
            valid = false;
        }

        if (phone.trim().isEmpty()) {
            updateUserView.setPhoneError("Phone name cannot be empty");
            valid = false;
        }

        if (valid) {
            updateUserView.setFirstNameError(null);
            updateUserView.setLastNameError(null);
            updateUserView.setBirthdayError(null);
            updateUserView.setPhoneError(null);

            RequestParams params = new RequestParams();
            params.add("firstName", firstName);
            params.add("lastName", lastName);
            params.add("dob", birthday);
            params.add("gender", Gender.valueOf(gender).getGenderCode());
            params.add("phone", phone);

            updateUserRequest(params);
        }
    }

    private void updateUserRequest(RequestParams params) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);

        client.putMethod(updateUserView.requestContext(), Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.updateUserEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                updateUserView.showProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String message = response.getString("message");
                    updateUserView.hideProgressDialog();
                    updateUserView.showResultToast(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                try {
                    String message = errorResponse.getString("message");
                    updateUserView.hideProgressDialog();
                    updateUserView.showResultDialog(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
