package com.example.memoteandroidapp.Views;

import android.app.ProgressDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.memoteandroidapp.Interfaces.IRegister;
import com.example.memoteandroidapp.Presenters.RegisterPresenter;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.Fragments.ResultDialogFragment;

public class RegisterActivity extends AppCompatActivity implements IRegister.View {

    private IRegister.Presenter registerPresenter;

    private TextInputLayout registerUsername;
    private TextInputLayout registerPassword;
    private TextInputLayout registerRePassword;
    private TextInputLayout registerEmail;
    private TextInputLayout registerFirstName;
    private TextInputLayout registerLastName;
    private TextInputLayout registerBirthday;
    private Spinner registerGender;
    private TextInputLayout registerPhone;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Bind UI with variable
        registerUsername = findViewById(R.id.registerUsername);
        registerPassword = findViewById(R.id.registerPassword);
        registerRePassword = findViewById(R.id.registerRePassword);
        registerEmail = findViewById(R.id.registerEmail);
        registerFirstName = findViewById(R.id.registerFirstName);
        registerLastName = findViewById(R.id.registerLastName);
        registerBirthday = findViewById(R.id.registerDob);
        registerGender = findViewById(R.id.registerGender);
        registerPhone = findViewById(R.id.registerPhone);
        toolbar = findViewById(R.id.registerToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);

        //Create instance of Presenter
        registerPresenter = new RegisterPresenter(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void setUsernameError() {
        registerUsername.setError("Username");
    }

    @Override
    public void setPasswordError() {
        registerPassword.setError("Password");
    }

    @Override
    public void setRePasswordError() {
        registerRePassword.setError("Re-password");
    }

    @Override
    public void setEmailError() {
        registerEmail.setError("Email");
    }

    @Override
    public void setFirstNameError() {
        registerFirstName.setError("First Name");
    }

    @Override
    public void setLastNameError() {
        registerLastName.setError("Last Name");
    }

    @Override
    public void setDobError() {
        registerBirthday.setError("Birth day");
    }

    @Override
    public void setPhoneError() {
        registerPhone.setError("Phone");
    }

    @Override
    public void showProcessDialog() {
        progressDialog.setMessage("Registering");
        progressDialog.show();
    }

    @Override
    public void hideProcessDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showResultToast(String result) {
        Toast resultToast = Toast.makeText(this, result, Toast.LENGTH_LONG);
        resultToast.show();
        finish();
    }

    @Override
    public void showResultDialog(String result) {
        Bundle bundle = new Bundle();
        bundle.putString("title", "Sign Up");
        bundle.putString("message", result);
        DialogFragment resultFragment = new ResultDialogFragment();
        resultFragment.setArguments(bundle);
        resultFragment.show(getSupportFragmentManager(), "resultFragment");
    }

    public void onClickRegister(View v) {
        registerPresenter.registerUser(
                registerUsername.getEditText().getText().toString(),
                registerPassword.getEditText().getText().toString(),
                registerRePassword.getEditText().getText().toString(),
                registerEmail.getEditText().getText().toString(),
                registerFirstName.getEditText().getText().toString(),
                registerLastName.getEditText().getText().toString(),
                registerBirthday.getEditText().getText().toString(),
                registerGender.getSelectedItem().toString(),
                registerPhone.getEditText().getText().toString()
        );
    }
}
