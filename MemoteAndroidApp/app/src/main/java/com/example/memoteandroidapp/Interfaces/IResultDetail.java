package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface IResultDetail {
    interface View {
        void setResultFullName(String fullName);
        void setResultEmail(String email);
        void setResultBirthday(String birthday);
        void setResultGender(String gender);
        void setResultPhone(String phone);
        void setButton(boolean result);
        void showToastResult(String message);
        Context requestContext();
    }

    interface Presenter {
        void currentResultDetail();
        void checkRelation();
        void followUser();
        void unfollowUser();
    }
}
