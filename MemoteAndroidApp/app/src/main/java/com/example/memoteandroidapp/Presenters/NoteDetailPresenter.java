package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.INoteDetail;
import com.example.memoteandroidapp.Models.Comment;
import com.example.memoteandroidapp.Models.Note;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class NoteDetailPresenter implements INoteDetail.Presenter {

    private INoteDetail.View noteDetailView;
    private PersistentCookieStore cookieStore;

    public NoteDetailPresenter(INoteDetail.View noteDetailView) {
        this.noteDetailView = noteDetailView;
        cookieStore = new PersistentCookieStore(this.noteDetailView.requestContext());
    }

    @Override
    public void setContentDisplay() {
        noteDetailView.setNotePoster(CurrentSession.currentNote.getPoster().getFullName());
        noteDetailView.setNotePostedTime(CurrentSession.currentNote.getPostedTime());
        noteDetailView.setNoteLocation("at " + CurrentSession.currentNote.getLocAddress());
        noteDetailView.setNoteContent(CurrentSession.currentNote.getNoteContent());
    }

    @Override
    public void reloadNote() {
        noteDetailRequest();
    }

    @Override
    public void deleteNote() {
        deleteNoteRequest();
    }

    @Override
    public void addComment(String comment) {
        boolean valid = true;
        if (comment.trim().isEmpty()) {
            valid = false;
        }

        if (valid) {
            RequestParams params = new RequestParams();
            params.put("commentContent", comment);
            params.put("note", CurrentSession.currentNote.getNoteId());

            addCommentRequest(params, comment);
        }
    }

    @Override
    public void getComments() {
        CurrentSession.noteComments.clear();
        getCommentsRequest();
    }

    @Override
    public void deleteComment(int index) {
        deleteCommentRequest(index);
    }

    private void noteDetailRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(noteDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + "/" + CurrentSession.currentNote.getNoteId(), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject noteObject = response.getJSONObject("note");
                    Note note = Note.convertJSONObjectToNote(noteObject);

                    CurrentSession.currentNote = note;
                    setContentDisplay();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void deleteNoteRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.deleteMethod(noteDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + Endpoints.deleteNoteEndpoint + CurrentSession.currentNote.getNoteId(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String result = response.getString("message");
                    noteDetailView.showToastResult(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addCommentRequest(RequestParams params, final String comment) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.postMethod(noteDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseCmtURL + Endpoints.postCmtEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Comment newComment = new Comment();
                    newComment.setCommentId(response.getString("commentId"));
                    newComment.setCommentContent(comment);
                    newComment.setPoster(CurrentSession.currentUser);
                    newComment.setPostedTime("Just now");
                    newComment.setCommentId(CurrentSession.currentNote.getNoteId());

                    CurrentSession.noteComments.add(newComment);
                    noteDetailView.commentListDataChanged();

                    noteDetailView.setCommentContent("");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getCommentsRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(noteDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseCmtURL + Endpoints.getCmtEndpoint + CurrentSession.currentNote.getNoteId(), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray commentArray = response.getJSONArray("comment");
                    List<Comment> comments = Comment.convertJSONArrayToList(commentArray);

                    CurrentSession.noteComments.addAll(comments);

                    noteDetailView.commentListDataChanged();
                    noteDetailView.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void deleteCommentRequest(final int index) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.deleteMethod(noteDetailView.requestContext(), Endpoints.baseURL + Endpoints.baseCmtURL + Endpoints.deleteCmtEndpoint + CurrentSession.noteComments.get(index).getCommentId(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String message = response.getString("message");

                    CurrentSession.noteComments.remove(index);
                    noteDetailView.showToastResult(message);
                    noteDetailView.commentListDataChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
