package com.example.memoteandroidapp.Views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.memoteandroidapp.Adapters.CommentAdapter;
import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.INoteDetail;
import com.example.memoteandroidapp.Models.Note;
import com.example.memoteandroidapp.Presenters.NoteDetailPresenter;
import com.example.memoteandroidapp.R;

import java.util.ArrayList;
import java.util.Objects;

public class NoteDetailActivity extends AppCompatActivity implements INoteDetail.View {

    private INoteDetail.Presenter noteDetailPresenter;
    private TextView notePoster;
    private TextView notePostedTime;
    private TextView noteLocation;
    private TextView noteContent;
    private EditText comment;
    private ImageButton sendButton;
    private Toolbar toolbar;
    private RecyclerView listComment;
    private CommentAdapter commentAdapter;
    private SwipeRefreshLayout commentRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        noteDetailPresenter = new NoteDetailPresenter(this);
        CurrentSession.noteComments = new ArrayList<>();
        CurrentSession.currentNote = new Note();

        Intent data = getIntent();
        if (data.getIntExtra("requestCode", -1) == Constants.USER_NOTE_DETAIL_CODE) {
            CurrentSession.currentNote = CurrentSession.userNotes.get(data.getIntExtra("noteIndex", -1));
        }
        if (data.getIntExtra("requestCode", -1) == Constants.NEWSFEED_NOTE_DETAIL_CODE) {
            CurrentSession.currentNote = CurrentSession.newsFeedNotes.get(data.getIntExtra("noteIndex", -1));
        }

        notePoster = findViewById(R.id.noteDetailPoster);
        notePostedTime = findViewById(R.id.noteDetailPostedTime);
        noteLocation = findViewById(R.id.noteDetailLocation);
        noteContent = findViewById(R.id.noteDetailContent);
        comment = findViewById(R.id.noteDetailComment);
        sendButton = findViewById(R.id.noteDetailSendCmt);
        toolbar = findViewById(R.id.noteDetailToolBar);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        comment.addTextChangedListener(toggleButtonTextWatcher);
        sendButton.setVisibility(View.GONE);

        listComment = findViewById(R.id.noteDetailListComment);
        commentAdapter = new CommentAdapter(CurrentSession.noteComments, this);
        listComment.setLayoutManager(new LinearLayoutManager(this));
        listComment.setAdapter(commentAdapter);

        commentRefresh = findViewById(R.id.noteDetailCommentRefresh);
        commentRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                noteDetailPresenter.getComments();
            }
        });

        noteDetailPresenter.setContentDisplay();
        noteDetailPresenter.getComments();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (CurrentSession.currentNote.getPoster().getUserId().equals(CurrentSession.currentUser.getUserId())) {
            getMenuInflater().inflate(R.menu.menu_note_option, menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                CurrentSession.currentNote = null;
                onBackPressed();
                return true;
            }
            case R.id.optionEdit: {
                Intent editNote = new Intent(this, EditActivity.class);
                editNote.putExtra("option", Constants.CREATE_OPTION_EDIT_NOTE);
                startActivityForResult(editNote, Constants.REQUEST_NOTE_RELOAD);
                return true;
            }
            case R.id.optionDelete: {
                new AlertDialog.Builder(this)
                        .setTitle("Delete note")
                        .setMessage("Do you want to delete this note?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                noteDetailPresenter.deleteNote();
                                CurrentSession.currentNote = null;
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0: {
                noteDetailPresenter.deleteComment(item.getGroupId());
                return true;
            }
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_NOTE_RELOAD) {
            if (resultCode == Constants.REQUEST_RELOAD_AFTER_EDIT) {
                noteDetailPresenter.reloadNote();
            }
        }
    }

    @Override
    public void setNotePoster(String fullName) {
        notePoster.setText(fullName);
    }

    @Override
    public void setNotePostedTime(String time) {
        notePostedTime.setText(time);
    }

    @Override
    public void setNoteLocation(String location) {
        noteLocation.setText(location);
    }

    @Override
    public void setNoteContent(String content) {
        noteContent.setText(content);
    }

    @Override
    public void setCommentContent(String content) {
        if (content.equals("")) {
            comment.getText().clear();
        } else {
            comment.setText(content);
        }
    }

    @Override
    public void commentListDataChanged() {
        commentAdapter.notifyDataSetChanged();
    }

    @Override
    public void setRefreshing(boolean status) {
        commentRefresh.setRefreshing(status);
    }

    @Override
    public void showToastResult(String result) {
        Toast resultToast = Toast.makeText(this, result, Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    @Override
    public Activity requestActivity() {
        return this;
    }

    private TextWatcher toggleButtonTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (s.toString().trim().isEmpty()) {
                sendButton.setVisibility(View.GONE);
            } else {
                sendButton.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().trim().isEmpty()) {
                sendButton.setVisibility(View.GONE);
            } else {
                sendButton.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void onClickSendCmt(View view) {
        noteDetailPresenter.addComment(comment.getText().toString());
    }
}
