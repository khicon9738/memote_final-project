package com.example.memoteandroidapp.Views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Interfaces.IInit;
import com.example.memoteandroidapp.Presenters.InitPresenter;
import com.example.memoteandroidapp.R;
import com.loopj.android.http.PersistentCookieStore;

public class InitialActivity extends AppCompatActivity implements IInit.View {

    private IInit.Presenter initPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        initPresenter = new InitPresenter(this);
        initPresenter.checkPermission();
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    initPresenter.validateToken();
                }
            }, 5000);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_ACCESS_FINE_LOCATION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            initPresenter.validateToken();
                        }
                    }, 5000);
                } else {
                    initPresenter.checkPermission();
                }
                break;
            }
        }
    }

    @Override
    public void showResultToast(String result) {
        Toast resultToast = Toast.makeText(this, result, Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public void redirectToLogin() {
        Intent login = new Intent(this, LoginActivity.class);
        startActivity(login);
        finish();
    }

    @Override
    public void redirectToHomePage() {
        Intent homePage = new Intent(this, MainActivity.class);
        startActivity(homePage);
        finish();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    @Override
    public Activity requestActivity() {
        return this;
    }
}
