package com.example.memoteandroidapp.Views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.memoteandroidapp.Interfaces.ILogin;
import com.example.memoteandroidapp.Presenters.LoginPresenter;
import com.example.memoteandroidapp.R;

public class LoginActivity extends AppCompatActivity implements ILogin.View {

    private ILogin.Presenter loginPresenter;
    private TextInputLayout loginUsername;
    private TextInputLayout loginPassword;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginUsername = findViewById(R.id.loginUsername);
        loginPassword = findViewById(R.id.loginPassword);
        progressDialog = new ProgressDialog(this);

        loginPresenter = new LoginPresenter(this);
    }

    @Override
    public void showUsernameError(String error) {
        loginUsername.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        loginPassword.setError(error);
    }

    @Override
    public void showProgressBar() {
        progressDialog.setMessage("Log in");
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void redirectToRegister(View view) {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    @Override
    public void redirectToHomePage() {
        Intent homeIntent = new Intent(this,MainActivity.class);
        startActivity(homeIntent);
        finish();
    }

    @Override
    public void showResultToast(String message) {
        Toast resultToast = Toast.makeText(this,message,Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    public void onClickLogin(View v) {
        loginPresenter.loginUser(loginUsername.getEditText().getText().toString(),
                loginPassword.getEditText().getText().toString());
    }
}
