package com.example.memoteandroidapp.Common;

import com.example.memoteandroidapp.Models.Comment;
import com.example.memoteandroidapp.Models.Note;
import com.example.memoteandroidapp.Models.User;

import java.util.List;

public class CurrentSession {

    public static User currentUser;
    public static Note currentNote;
    public static List<Note> userNotes;
    public static List<Note> newsFeedNotes;
    public static List<Comment> noteComments;
    public static List<User> userSearchResults;
    public static User currentResult;
    public static String noteRange = "500";
}
