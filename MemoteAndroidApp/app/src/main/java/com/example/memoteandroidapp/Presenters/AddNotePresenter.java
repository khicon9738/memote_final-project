package com.example.memoteandroidapp.Presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IAddNote;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class AddNotePresenter implements IAddNote.Presenter {

    private IAddNote.View addNoteView;
    private PersistentCookieStore cookieStore;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback requestUpdateCallback;
    private Location currentLoc;
    private String currentAddress;

    public AddNotePresenter(IAddNote.View view) {
        this.addNoteView = view;

        cookieStore = new PersistentCookieStore(addNoteView.requestContext());
        fusedLocationProviderClient = new FusedLocationProviderClient(addNoteView.requestContext());

        locationRequest = new LocationRequest();
        locationRequest.setInterval(60000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        requestUpdateCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                } else {
                    currentLoc = locationResult.getLastLocation();
                    currentAddress = getCurrentAddress();
                    addNoteView.setCurrentLocation(currentAddress);
                }
            }
        };

        checkPermission();
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, requestUpdateCallback, null);
        requestLastLocation(fusedLocationProviderClient.getLastLocation());
    }

    @Override
    public void addNote(String currentLocation, String noteContent) {
        boolean valid = true;
        if (noteContent.trim().isEmpty()) {
            addNoteView.showNoteContentError("Content cannot be empty");
            valid = false;
        }

        if (valid) {
            addNoteView.showNoteContentError(null);

            String geometryString = "{\"type\": \"Point\", \"coordinates\": [" + currentLoc.getLongitude() + ", " + currentLoc.getLatitude() + "]}";
            RequestParams params = new RequestParams();
            params.put("noteContent", noteContent);
            params.put("geometry", geometryString);
            params.put("locAddress", currentLocation.isEmpty() ? currentAddress : currentLocation);

            addNoteRequest(params);
        }
    }

    private void addNoteRequest(RequestParams params) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.postMethod(addNoteView.requestContext(), Endpoints.baseURL + Endpoints.baseNoteURL + Endpoints.pinNoteEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                fusedLocationProviderClient.removeLocationUpdates(requestUpdateCallback);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String message = response.getString("message");
                    addNoteView.showToastResult(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(errorResponse);
            }
        });
    }

    private void requestLastLocation(Task<Location> task) {
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLoc = location;
                    currentAddress = getCurrentAddress();
                    addNoteView.setCurrentLocation(currentAddress);
                }
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                currentLoc = null;
            }
        });
    }

    private String getCurrentAddress() {
        if (addNoteView.requestContext() == null) {
            return null;
        }
        Geocoder gcd = new Geocoder(addNoteView.requestContext(), Locale.getDefault());
        List<Address> addresses;

        try {
            addresses = gcd.getFromLocation(currentLoc.getLatitude(), currentLoc.getLongitude(), 10);
            currentAddress = addresses.get(0).getSubThoroughfare() + ", "
                    + addresses.get(0).getThoroughfare() + ", "
                    + addresses.get(0).getLocality() + ", "
                    + addresses.get(0).getSubAdminArea() + ", "
                    + addresses.get(0).getAdminArea() + ", "
                    + addresses.get(0).getCountryName();
            currentAddress = currentAddress.replaceAll(", null", "");
            currentAddress = currentAddress.replaceAll("null, ", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentAddress;
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(addNoteView.requestContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(addNoteView.requestActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.REQUEST_ACCESS_FINE_LOCATION_CODE);
        }
        if (ContextCompat.checkSelfPermission(addNoteView.requestContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(addNoteView.requestActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.REQUEST_ACCESS_COARSE_LOCATION_CODE);
        }
    }
}
