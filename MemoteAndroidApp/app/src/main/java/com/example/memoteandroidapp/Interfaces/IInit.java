package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

public interface IInit {
    interface View {
        void showResultToast(String result);
        void redirectToLogin();
        void redirectToHomePage();
        Context requestContext();
        Activity requestActivity();
    }

    interface Presenter {
        void validateToken();
        void checkPermission();
    }
}
