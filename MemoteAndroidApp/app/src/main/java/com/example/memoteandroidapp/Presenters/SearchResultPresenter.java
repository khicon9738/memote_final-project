package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.ISearchResult;
import com.example.memoteandroidapp.Models.User;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class SearchResultPresenter implements ISearchResult.Presenter {

    private ISearchResult.View searchResultView;
    private PersistentCookieStore cookieStore;

    public SearchResultPresenter(ISearchResult.View searchResultView) {
        this.searchResultView = searchResultView;

        cookieStore = new PersistentCookieStore(this.searchResultView.requestContext());
    }

    @Override
    public void searchUser(String keyword) {
        CurrentSession.userSearchResults.clear();
        searchResultRequest(keyword);
    }

    private void searchResultRequest(String keyword) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(searchResultView.requestContext(), Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.searchUserEndpoint + keyword, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray usersArray = response.getJSONArray("user");
                    List<User> users = User.convertJSONArrayToList(usersArray);

                    CurrentSession.userSearchResults.addAll(users);
                    searchResultView.searchResultNotifyChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
