package com.example.memoteandroidapp.Views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.IResultDetail;
import com.example.memoteandroidapp.Models.User;
import com.example.memoteandroidapp.Presenters.ResultDetailPresenter;
import com.example.memoteandroidapp.R;

public class ResultDetailActivity extends AppCompatActivity implements IResultDetail.View {

    private IResultDetail.Presenter resultViewPresenter;
    private TextView resultFullName;
    private TextView resultEmail;
    private TextView resultDob;
    private TextView resultGender;
    private TextView resultPhone;
    private Button followBtn;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_detail);
        resultViewPresenter = new ResultDetailPresenter(this);
        CurrentSession.currentResult = new User();

        Intent data = getIntent();
        CurrentSession.currentResult = CurrentSession.userSearchResults.get(data.getIntExtra("resultIndex", -1));

        resultFullName = findViewById(R.id.resultDetailFullName);
        resultEmail = findViewById(R.id.resultDetailEmail);
        resultDob = findViewById(R.id.resultDetailBirthday);
        resultGender = findViewById(R.id.resultDetailGender);
        resultPhone = findViewById(R.id.resultDetailPhone);
        followBtn = findViewById(R.id.resultDetailFollowBtn);

        if(CurrentSession.currentUser.getUserId().equals(CurrentSession.currentResult.getUserId())){
            followBtn.setVisibility(View.INVISIBLE);
        }

        toolbar = findViewById(R.id.resultDetailToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resultViewPresenter.currentResultDetail();
        resultViewPresenter.checkRelation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                CurrentSession.currentResult = null;
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void setResultFullName(String fullName) {
        resultFullName.setText("Full name: " + fullName);
    }

    @Override
    public void setResultEmail(String email) {
        resultEmail.setText("Email: " + email);
    }

    @Override
    public void setResultBirthday(String birthday) {
        resultDob.setText("Dob: " + birthday);
    }

    @Override
    public void setResultGender(String gender) {
        resultGender.setText("Gender: " + gender);
    }

    @Override
    public void setResultPhone(String phone) {
        resultPhone.setText("Phone: " + phone);
    }

    @Override
    public void setButton(boolean result) {
        if (result) {
            followBtn.setText(getString(R.string.result_detail_unfollow));
            followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickUnfollow(v);
                }
            });
        } else {
            followBtn.setText(getString(R.string.result_detail_follow));
            followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickFollow(v);
                }
            });
        }
    }

    @Override
    public void showToastResult(String message) {
        Toast resultToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    private void onClickFollow(View view) {
        resultViewPresenter.followUser();
    }

    private void onClickUnfollow(View view) {
        resultViewPresenter.unfollowUser();
    }
}
