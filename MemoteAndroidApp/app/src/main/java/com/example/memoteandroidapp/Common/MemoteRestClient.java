package com.example.memoteandroidapp.Common;

import android.content.Context;

import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import java.util.List;

public class MemoteRestClient {

    private AsyncHttpClient client;

    public MemoteRestClient(List<HeaderDto> headers) {
        this.client = new AsyncHttpClient();
        if (!headers.isEmpty()) {
            for (HeaderDto header : headers) {
                client.addHeader(header.getHeaderKey(), header.getHeaderValue());
            }
        }
    }

    public void setCookieStore (PersistentCookieStore store){
        this.client.setCookieStore(store);
    }

    //Post
    public void postMethod(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(context, url, params, responseHandler);
    }

    //Get
    public void getMethod(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(context, url, params, responseHandler);
    }

    //Put
    public void putMethod(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.put(context, url, params, responseHandler);
    }

    //Delete
    public void deleteMethod(Context context, String url, AsyncHttpResponseHandler responseHandler) {
        client.delete(context, url, responseHandler);
    }
}
