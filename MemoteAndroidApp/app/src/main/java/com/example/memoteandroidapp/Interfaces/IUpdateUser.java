package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface IUpdateUser {
    interface View {
        void setCurrentInfo(String firstName, String lastName, String birthday, String gender, String phone);
        void setFirstNameError(String error);
        void setLastNameError(String error);
        void setBirthdayError(String error);
        void setPhoneError(String error);
        void showProgressDialog();
        void hideProgressDialog();
        void showResultDialog(String message);
        void showResultToast(String message);
        Context requestContext();
    }

    interface Presenter {
        void updateUser(String firstName, String lastName, String birthday, String gender, String phone);
    }
}
