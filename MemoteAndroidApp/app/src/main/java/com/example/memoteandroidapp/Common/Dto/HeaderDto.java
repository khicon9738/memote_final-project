package com.example.memoteandroidapp.Common.Dto;

public class HeaderDto {
    private String headerKey;
    private String headerValue;

    public HeaderDto() {
    }

    public HeaderDto(String headerKey, String headerValue) {
        this.headerKey = headerKey;
        this.headerValue = headerValue;
    }

    public String getHeaderKey() {
        return headerKey;
    }

    public void setHeaderKey(String headerKey) {
        this.headerKey = headerKey;
    }

    public String getHeaderValue() {
        return headerValue;
    }

    public void setHeaderValue(String headerValue) {
        this.headerValue = headerValue;
    }
}
