package com.example.memoteandroidapp.Presenters;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IInit;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class InitPresenter implements IInit.Presenter {

    private IInit.View initView;
    private PersistentCookieStore cookieStore;

    public InitPresenter(IInit.View initView) {
        this.initView = initView;
        cookieStore = new PersistentCookieStore(initView.requestContext());
    }

    @Override
    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(initView.requestContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(initView.requestActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.REQUEST_ACCESS_FINE_LOCATION_CODE);
        }
    }

    @Override
    public void validateToken() {
        if(cookieStore.getCookies().isEmpty()){
            initView.redirectToLogin();
        }else{
            validateTokenRequest();
        }
    }

    private void validateTokenRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(new HeaderDto("Content-Type", "application/x-www-form-urlencoded"));
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.postMethod(initView.requestContext(), Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.verifyJWTEndpoint, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                initView.redirectToHomePage();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                cookieStore.clear();
                initView.showResultToast("Session expired! Please login again");
                initView.redirectToLogin();
            }
        });
    }
}
