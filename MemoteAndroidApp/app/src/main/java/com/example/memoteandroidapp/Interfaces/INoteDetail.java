package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

public interface INoteDetail {
    interface View {
        void setNotePoster(String fullName);
        void setNotePostedTime(String time);
        void setNoteLocation(String location);
        void setNoteContent(String content);
        void setCommentContent(String content);
        void commentListDataChanged();
        void setRefreshing(boolean status);
        void showToastResult(String result);
        Context requestContext();
        Activity requestActivity();
    }

    interface Presenter {
        void setContentDisplay();
        void addComment(String comment);
        void reloadNote();
        void deleteNote();
        void getComments();
        void deleteComment(int index);
    }
}
