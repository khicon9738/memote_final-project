package com.example.memoteandroidapp.Views;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.memoteandroidapp.Interfaces.IUpdateUser;
import com.example.memoteandroidapp.Presenters.UpdateUserPresenter;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.Fragments.ResultDialogFragment;

public class UpdateUserActivity extends AppCompatActivity implements IUpdateUser.View {

    private IUpdateUser.Presenter updateUserPresenter;

    private TextInputLayout updateFirstName;
    private TextInputLayout updateLastName;
    private TextInputLayout updateBirthday;
    private Spinner updateGender;
    private TextInputLayout updatePhone;
    private ProgressDialog progressDialog;
    private DialogFragment resultDialog;
    private Toolbar updateToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        updateFirstName = findViewById(R.id.updateFirstName);
        updateLastName = findViewById(R.id.updateLastName);
        updateBirthday = findViewById(R.id.updateBirthday);
        updateGender = findViewById(R.id.updateGender);
        updatePhone = findViewById(R.id.updatePhone);
        updateToolbar = findViewById(R.id.updateToolbar);
        setSupportActionBar(updateToolbar);
        getSupportActionBar().setTitle(getString(R.string.update_user_act_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);

        updateUserPresenter = new UpdateUserPresenter(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void setCurrentInfo(String firstName, String lastName, String birthday, String gender, String phone) {
        updateFirstName.getEditText().setText(firstName);
        updateLastName.getEditText().setText(lastName);
        updateBirthday.getEditText().setText(birthday);
        updateGender.setSelection(Integer.parseInt(gender));
        updatePhone.getEditText().setText(phone);
    }

    @Override
    public void setFirstNameError(String error) {
        updateFirstName.setError(error);
    }

    @Override
    public void setLastNameError(String error) {
        updateLastName.setError(error);
    }

    @Override
    public void setBirthdayError(String error) {
        updateBirthday.setError(error);
    }

    @Override
    public void setPhoneError(String error) {
        updatePhone.setError(error);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage("Updating...");
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showResultDialog(String message) {
        Bundle bundle = new Bundle();
        bundle.putString("title", "Update information");
        bundle.putString("message", message);
        resultDialog = new ResultDialogFragment();
        resultDialog.setArguments(bundle);
        resultDialog.show(getSupportFragmentManager(), "resultDialog");
    }

    @Override
    public void showResultToast(String message) {
        Toast resultToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        resultToast.show();
        finish();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    public void onClickUpdateBtn(View view) {
        updateUserPresenter.updateUser(
                updateFirstName.getEditText().getText().toString(),
                updateLastName.getEditText().getText().toString(),
                updateBirthday.getEditText().getText().toString(),
                updateGender.getSelectedItem().toString(),
                updatePhone.getEditText().getText().toString()
        );
    }
}
