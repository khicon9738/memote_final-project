package com.example.memoteandroidapp.Views.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.memoteandroidapp.Adapters.NoteAdapter;
import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.IUserDetail;
import com.example.memoteandroidapp.Presenters.UserDetailPresenter;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.ChangePasswordActivity;
import com.example.memoteandroidapp.Views.LoginActivity;
import com.example.memoteandroidapp.Views.UpdateUserActivity;

import java.util.ArrayList;

public class UserDetailFragment extends Fragment implements IUserDetail.View {

    private View view;
    private IUserDetail.Presenter userDetailPresenter;
    private TextView userFullName;
    private TextView userEmail;
    private TextView userBirthday;
    private TextView userGender;
    private TextView userPhone;
    private TextView updateProfile;
    private TextView changePassword;
    private TextView logout;
    private RecyclerView userNoteList;
    private NoteAdapter noteAdapter;
    private SwipeRefreshLayout refreshNoteList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_detail, container, false);
        userDetailPresenter = new UserDetailPresenter(this);
        CurrentSession.userNotes = new ArrayList<>();

        userFullName = view.findViewById(R.id.userDetailFullName);
        userEmail = view.findViewById(R.id.userDetailEmail);
        userBirthday = view.findViewById(R.id.userDetailBirthday);
        userGender = view.findViewById(R.id.userDetailGender);
        userPhone = view.findViewById(R.id.userDetailPhone);
        updateProfile = view.findViewById(R.id.userDetailUpdateBtn);
        changePassword = view.findViewById(R.id.userDetailChangePwdBtn);
        logout = view.findViewById(R.id.userDetailLogoutBtn);

        userNoteList = view.findViewById(R.id.userDetailNoteList);
        noteAdapter = new NoteAdapter(CurrentSession.userNotes, getActivity(), Constants.USER_NOTE_DETAIL_CODE);
        userNoteList.setLayoutManager(new LinearLayoutManager(getContext()));
        userNoteList.setAdapter(noteAdapter);

        refreshNoteList = view.findViewById(R.id.userDetailRefreshNotes);
        refreshNoteList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                userDetailPresenter.getUserNotes();
            }
        });

        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent updateAct = new Intent(getActivity(), UpdateUserActivity.class);
                getActivity().startActivity(updateAct);
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent changePasswordAct = new Intent(getActivity(), ChangePasswordActivity.class);
                getActivity().startActivity(changePasswordAct);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetailPresenter.logOut();
            }
        });

        userDetailPresenter.getUserDetail();
        userDetailPresenter.getUserNotes();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        userDetailPresenter.getUserDetail();
    }

    @Override
    public void setRefreshing(boolean status) {
        refreshNoteList.setRefreshing(status);
    }

    @Override
    public void userNoteDataChanged() {
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    public void setUserFullName(String fullName) {
        userFullName.setText("Full name: " + fullName);
    }

    @Override
    public void setUserEmail(String email) {
        userEmail.setText("Email: " + email);
    }

    @Override
    public void setUserBirthday(String birthday) {
        userBirthday.setText("Birthday: " + birthday);
    }

    @Override
    public void setUserGender(String gender) {
        userGender.setText("Gender: " + gender);
    }

    @Override
    public void setUserPhone(String phone) {
        userPhone.setText("Phone: " + phone);
    }

    @Override
    public void redirectLogin() {
        Intent login = new Intent(getActivity(), LoginActivity.class);
        getActivity().startActivity(login);
        getActivity().finish();
    }

    @Override
    public void showResultToast(String message) {
        Toast resultToast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public Context requestContext() {
        return super.getContext();
    }
}