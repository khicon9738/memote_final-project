package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IResultDetail;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class ResultDetailPresenter implements IResultDetail.Presenter {

    private IResultDetail.View resultView;
    private PersistentCookieStore cookieStore;

    public ResultDetailPresenter(IResultDetail.View resultView) {
        this.resultView = resultView;

        cookieStore = new PersistentCookieStore(this.resultView.requestContext());
    }

    @Override
    public void checkRelation() {
        checkRelationRequest();
    }

    @Override
    public void currentResultDetail() {
        resultView.setResultFullName(CurrentSession.currentResult.getFullName());
        resultView.setResultEmail(CurrentSession.currentResult.getEmail());
        resultView.setResultBirthday(CurrentSession.currentResult.getDob());
        resultView.setResultGender(CurrentSession.currentResult.getGender().toString());
        resultView.setResultPhone(CurrentSession.currentResult.getPhone());
    }

    @Override
    public void followUser() {
        RequestParams params = new RequestParams();
        params.put("following", CurrentSession.currentResult.getUserId());
        followUserRequest(params);
    }

    @Override
    public void unfollowUser() {
        unfollowUserRequest();
    }

    private void checkRelationRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.getMethod(resultView.requestContext(), Endpoints.baseURL + Endpoints.baseRelationURL + Endpoints.checkRelation + CurrentSession.currentResult.getUserId(), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    boolean result = response.getBoolean("result");

                    resultView.setButton(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void followUserRequest(RequestParams params) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.postMethod(resultView.requestContext(), Endpoints.baseURL + Endpoints.baseRelationURL + Endpoints.followEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String message = response.getString("message");

                    resultView.showToastResult(message);
                    checkRelationRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void unfollowUserRequest() {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.deleteMethod(resultView.requestContext(), Endpoints.baseURL + Endpoints.baseRelationURL + Endpoints.unFollowEndpoint + CurrentSession.currentResult.getUserId(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String message = response.getString("message");

                    resultView.showToastResult(message);
                    checkRelationRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
