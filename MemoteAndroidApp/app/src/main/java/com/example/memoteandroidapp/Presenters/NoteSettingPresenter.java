package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.CurrentSession;
import com.example.memoteandroidapp.Interfaces.INoteSetting;

public class NoteSettingPresenter implements INoteSetting.Presenter {

    private INoteSetting.View noteSettingView;

    public NoteSettingPresenter(INoteSetting.View noteSettingView) {
        this.noteSettingView = noteSettingView;
    }


    @Override
    public void loadSetting() {
        noteSettingView.setRange(CurrentSession.noteRange);
    }

    @Override
    public void saveSetting(String newRange) {
        try {
            noteSettingView.setRangeError(null);

            if (newRange.trim().isEmpty()) {
                noteSettingView.setRangeError("Range cannot be empty");
            }

            long range = Long.parseLong(newRange);

            if (0 > range | range > 20000) {
                noteSettingView.setRangeError("The range must from 500 to 20000");
            } else {
                CurrentSession.noteRange = String.valueOf(range);
                noteSettingView.showResultToast("Range saved! Please reload newsfeed");
            }
        } catch (NumberFormatException numberFormat) {
            noteSettingView.setRangeError("Range must be number");
        }
    }
}
