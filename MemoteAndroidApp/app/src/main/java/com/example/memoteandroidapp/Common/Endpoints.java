package com.example.memoteandroidapp.Common;

public final class Endpoints {
    public static String baseURL = "http://54.169.251.218:4200";

    //User endpoints
    public static String baseUserURL = "/api/users";
    public static String registerEndpoint = "/register";
    public static String loginEndpoint = "/login";
    public static String userDetailEndpoint = "/user-detail";
    public static String updateUserEndpoint = "/update-detail";
    public static String verifyJWTEndpoint = "/verify-jwt";
    public static String changePasswordEndpoint = "/change-password";
    public static String searchUserEndpoint = "/search?keyword=";

    //Note endpoints
    public static String baseNoteURL = "/api/notes";
    public static String pinNoteEndpoint = "/pin-note";
    public static String getNoteEndpoint = "/load-notes?long=%s&lat=%s&minDis=%s&maxDis=%s";
    public static String userNoteEndpoint = "/user-notes";
    public static String editNoteEndpoint = "/update-note/";
    public static String deleteNoteEndpoint = "/delete-note/";

    //Comment endpoints
    public static String baseCmtURL = "/api/comments";
    public static String postCmtEndpoint = "/post-comment";
    public static String getCmtEndpoint = "/get-comments?noteId=";
    public static String deleteCmtEndpoint = "/delete-comment/";

    //Relation endpoints
    public static String baseRelationURL = "/api/relations";
    public static String followEndpoint = "/follow";
    public static String unFollowEndpoint = "/unfollow/";
    public static String checkRelation = "?following=";
}
