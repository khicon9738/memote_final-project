package com.example.memoteandroidapp.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Models.Note;
import com.example.memoteandroidapp.R;
import com.example.memoteandroidapp.Views.NoteDetailActivity;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private List<Note> noteList;
    private Activity activity;
    private int createOption;

    public NoteAdapter(List<Note> noteList, Activity activity, int createOption) {
        this.noteList = noteList;
        this.activity = activity;
        this.createOption = createOption;
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {

        private TextView poster;
        private TextView postedTime;
        private TextView location;
        private TextView content;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.notePoster);
            postedTime = itemView.findViewById(R.id.notePostedTime);
            location = itemView.findViewById(R.id.noteLocation);
            content = itemView.findViewById(R.id.noteContent);
        }
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View noteItem = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_note_item, viewGroup, false);
        return new NoteViewHolder(noteItem);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int i) {
        final int noteIndex = i;
        final Note note = noteList.get(i);
        noteViewHolder.poster.setText(note.getPoster().getFullName());
        noteViewHolder.postedTime.setText(note.getPostedTime());
        noteViewHolder.location.setText(String.format("at %s", note.getLocAddress()));
        noteViewHolder.content.setText(note.getNoteContent());

        if(createOption == Constants.USER_NOTE_DETAIL_CODE){
            noteViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent noteDetail = new Intent(activity, NoteDetailActivity.class);
                    noteDetail.putExtra("requestCode", createOption);
                    noteDetail.putExtra("noteIndex", noteIndex);
                    activity.startActivity(noteDetail);
                }
            });
        }else{
            noteViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent noteDetail = new Intent(activity, NoteDetailActivity.class);
                    noteDetail.putExtra("requestCode", createOption);
                    noteDetail.putExtra("noteIndex", noteIndex);
                    activity.startActivity(noteDetail);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }
}
