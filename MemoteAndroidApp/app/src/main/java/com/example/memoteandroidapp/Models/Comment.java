package com.example.memoteandroidapp.Models;

import com.example.memoteandroidapp.Common.Converter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Comment {

    private String commentId;
    private String commentContent;
    private User poster;
    private String postedTime;

    public Comment() {
    }

    public Comment(String commentId, String commentContent, User poster, String postedTime) {
        this.commentId = commentId;
        this.commentContent = commentContent;
        this.poster = poster;
        this.postedTime = postedTime;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public User getPoster() {
        return poster;
    }

    public void setPoster(User poster) {
        this.poster = poster;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }

    public static Comment convertJSONObjectToComment(JSONObject data) throws JSONException {
        Comment comment = new Comment();

        comment.setCommentId(data.getString("_id"));
        comment.setCommentContent(data.getString("commentContent"));
        comment.setPoster(User.convertJSONObjectToUser(data.getJSONObject("poster")));
        comment.setPostedTime(Converter.convertISOToShortDate(data.getString("createdAt")));

        return comment;
    }

    public static List<Comment> convertJSONArrayToList(JSONArray data) throws JSONException {
        List<Comment> comments = new ArrayList<>();

        for (int i = 0; i <= data.length() - 1; i++) {
            Comment comment = Comment.convertJSONObjectToComment(data.getJSONObject(i));
            comments.add(comment);
        }

        return comments;
    }
}
