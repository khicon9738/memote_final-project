package com.example.memoteandroidapp.Views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Interfaces.IEdit;
import com.example.memoteandroidapp.Presenters.EditPresenter;
import com.example.memoteandroidapp.R;

public class EditActivity extends AppCompatActivity implements IEdit.View {

    private IEdit.Presenter editPresenter;
    private int createOption;
    private TextInputLayout editContent;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        editContent = findViewById(R.id.editContent);
        toolbar = findViewById(R.id.editToolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);

        Intent option = getIntent();
        createOption = option.getIntExtra("option", 0);

        if (createOption == Constants.CREATE_OPTION_EDIT_NOTE) {
            getSupportActionBar().setTitle("Edit note");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        editPresenter = new EditPresenter(this, createOption);
        editPresenter.setDisplayContent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void setEditContent(String content) {
        editContent.getEditText().setText(content);
    }

    @Override
    public void showContentError(String error) {
        editContent.setError(error);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage("Updating...");
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToastResult(String result) {
        Toast resultToast = Toast.makeText(this, result, Toast.LENGTH_LONG);
        resultToast.show();
        if (createOption == Constants.CREATE_OPTION_EDIT_NOTE) {
            setResult(Constants.REQUEST_RELOAD_AFTER_EDIT, null);
        }
        this.finish();
    }

    @Override
    public Context requestContext() {
        return this;
    }

    public void onClickUpdate(View view) {
        editPresenter.updateContent(editContent.getEditText().getText().toString());
    }
}
