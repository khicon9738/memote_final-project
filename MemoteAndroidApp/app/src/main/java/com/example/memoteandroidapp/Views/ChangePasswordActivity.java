package com.example.memoteandroidapp.Views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Interfaces.IChangePassword;
import com.example.memoteandroidapp.Presenters.ChangePasswordPresenter;
import com.example.memoteandroidapp.R;

public class ChangePasswordActivity extends AppCompatActivity implements IChangePassword.View {

    private IChangePassword.Presenter changePasswordPresenter;
    private TextInputLayout currentPassword;
    private TextInputLayout newPassword;
    private TextInputLayout reNewPassword;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        currentPassword = findViewById(R.id.changePasswordCurrentPassword);
        newPassword = findViewById(R.id.changePasswordNewPassword);
        reNewPassword = findViewById(R.id.changePasswordReNewPassword);
        toolbar = findViewById(R.id.changePasswordToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.change_password_act_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        changePasswordPresenter = new ChangePasswordPresenter(this);
        progressDialog = new ProgressDialog(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void showCurrentPasswordError(String error) {
        currentPassword.setError(error);
    }

    @Override
    public void showNewPasswordError(String error) {
        newPassword.setError(error);
    }

    @Override
    public void showReNewPasswordError(String error) {
        reNewPassword.setError(error);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage("Changing password...");
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToastResult(String message) {
        Toast resultToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        resultToast.show();
    }

    @Override
    public void redirectToLogin() {
        Intent loginAct = new Intent(this, LoginActivity.class);
        loginAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        loginAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginAct);
        finish();
    }


    @Override
    public Context requestContext() {
        return this;
    }

    public void onClickReset(View view) {
        currentPassword.getEditText().setText(null);
        newPassword.getEditText().setText(null);
        reNewPassword.getEditText().setText(null);
    }

    public void onClickChangePwd(View view) {
        changePasswordPresenter.changePassword(
                currentPassword.getEditText().getText().toString(),
                newPassword.getEditText().getText().toString(),
                reNewPassword.getEditText().getText().toString());
    }
}
