package com.example.memoteandroidapp.Interfaces;

import android.app.Activity;
import android.content.Context;

public interface IMain {
    interface View{
        Context requestContext();
        Activity requestActivity();
    }
    interface Presenter{
    }
}
