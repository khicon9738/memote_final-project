package com.example.memoteandroidapp.Interfaces;

import android.content.Context;

public interface IEdit {
    interface View{
        void setEditContent(String content);
        void showContentError(String error);
        void showProgressDialog();
        void hideProgressDialog();
        void showToastResult(String result);
        Context requestContext();
    }

    interface Presenter{
        void setDisplayContent();
        void updateContent(String content);
    }
}
