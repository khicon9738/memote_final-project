package com.example.memoteandroidapp.Common.Enum;

public enum Gender {
    Male("0"),
    Female("1"),
    Unknown("2");

    private final String genderCode;

    Gender(String genderCode){
        this.genderCode = genderCode;
    }

    public String getGenderCode() {
        return genderCode;
    }
}
