package com.example.memoteandroidapp.Common;

import com.example.memoteandroidapp.Common.Dto.HeaderDto;

public final class Constants {
    public final static HeaderDto HEADER_CONTENT_TYPE_URLENCODED = new HeaderDto("Content-Type", "application/x-www-form-urlencoded");
    public final static int REQUEST_ACCESS_FINE_LOCATION_CODE = 1;
    public final static int REQUEST_ACCESS_COARSE_LOCATION_CODE = 2;
    public final static int USER_NOTE_DETAIL_CODE = 1;
    public final static int NEWSFEED_NOTE_DETAIL_CODE = 2;
    public final static int CREATE_OPTION_EDIT_NOTE = 1;
    public final static int CREATE_OPTION_EDIT_COMMENT = 2;
    public final static int REQUEST_NOTE_RELOAD = 0;
    public final static int REQUEST_RELOAD_AFTER_EDIT = 1;
}
