package com.example.memoteandroidapp.Presenters;

import com.example.memoteandroidapp.Common.Constants;
import com.example.memoteandroidapp.Common.Dto.HeaderDto;
import com.example.memoteandroidapp.Common.Endpoints;
import com.example.memoteandroidapp.Common.MemoteRestClient;
import com.example.memoteandroidapp.Interfaces.IChangePassword;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

public class ChangePasswordPresenter implements IChangePassword.Presenter {

    private IChangePassword.View changePasswordView;
    private PersistentCookieStore cookieStore;

    public ChangePasswordPresenter(IChangePassword.View changePasswordView) {
        this.changePasswordView = changePasswordView;
        cookieStore = new PersistentCookieStore(changePasswordView.requestContext());
    }

    @Override
    public void changePassword(String currentPassword, String newPassword, String reNewPassword) {
        changePasswordView.showCurrentPasswordError(null);
        changePasswordView.showNewPasswordError(null);
        changePasswordView.showReNewPasswordError(null);

        boolean valid = true;

        if (currentPassword.trim().isEmpty()) {
            changePasswordView.showCurrentPasswordError("Current password cannot be empty.");
            valid = false;
        }
        if (newPassword.trim().isEmpty()) {
            changePasswordView.showNewPasswordError("New password cannot be empty.");
            valid = false;
        }
        if (reNewPassword.trim().isEmpty()) {
            changePasswordView.showReNewPasswordError("Re-enter password cannot be empty.");
            valid = false;
        }

        if (!newPassword.trim().equals(reNewPassword.trim())) {
            changePasswordView.showReNewPasswordError("Re-enter password is not match.");
            valid = false;
        }

        if(currentPassword.trim().equals(newPassword.trim())){
            changePasswordView.showNewPasswordError("New password must differ from old password.");
            valid = false;
        }

        if (valid) {
            changePasswordView.showCurrentPasswordError(null);
            changePasswordView.showNewPasswordError(null);
            changePasswordView.showReNewPasswordError(null);

            RequestParams params = new RequestParams();
            params.add("currentPassword", currentPassword);
            params.add("newPassword", newPassword);

            changePasswordRequest(params);
        }
    }

    private void changePasswordRequest(RequestParams params) {
        Cookie cookie = cookieStore.getCookies().get(0);
        List<HeaderDto> headers = new ArrayList<>();
        headers.add(Constants.HEADER_CONTENT_TYPE_URLENCODED);
        headers.add(new HeaderDto(cookie.getName(), cookie.getValue()));

        MemoteRestClient client = new MemoteRestClient(headers);
        client.putMethod(changePasswordView.requestContext(), Endpoints.baseURL + Endpoints.baseUserURL + Endpoints.changePasswordEndpoint, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                changePasswordView.showCurrentPasswordError(null);
                changePasswordView.showNewPasswordError(null);
                changePasswordView.showReNewPasswordError(null);
                changePasswordView.showProgressDialog();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    cookieStore.clear();
                    String resultMessage = response.getString("message");
                    changePasswordView.hideProgressDialog();
                    changePasswordView.showToastResult(resultMessage);
                    changePasswordView.redirectToLogin();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                try {
                    String resultMessage = errorResponse.getString("message");
                    changePasswordView.hideProgressDialog();
                    changePasswordView.showCurrentPasswordError(resultMessage);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
